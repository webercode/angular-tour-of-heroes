import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from '../interfaces/hero';

export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb(): { heroes: Hero[] } {
    return {
      heroes: [
        'Babe-o Wife-o',
        'Mr. Nice',
        'Narco',
        'Bombasto',
        'Celeritas',
        'Magneta',
        'Rubberman',
        'Dynama',
        'Dr. IQ',
        'Magma',
        'Tornado',
      ].map((name, i) => ({ id: i + 1, name }))
    };
  }

  genId(heroes: Hero[]) {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 1;
  }

}
